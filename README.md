# GView
Description: GView is a simple image browser for Mari. 

Install
------
Put contents of this repo into your $USER/Mari/Scripts directory.

**pyYAML** is required, you can get it here: http://pyyaml.org/

(Quick and dirty for home users: install **pyYAML** into your python directory, then copy ../site-packages/yaml into your ../Lib/site-packages/ folder in your Mari install)

Features
------
- File Browsing
    - Location bookmarks
- Thumbnail Previews
    - Thumbnail preview scaling
    - Expandable thumbnail generation "engines"
    - Includes OIIO*, Imagemagick*, Qt support.
    - Auto thumbnail generation toggle
- Playlists
    - Create custom "playlists" (arbitrary groups of images)
- Widget Styles
    - "Tab" mode
    - "Palette" mode
- Configuration
    - "Home" location
    - Cache location
    - Cache size preview
    - Cache copy option (when changing location)
    - Main widget style
    - Browser widget style (unfinished)
    - Default thumbnail engine


Getting Started
------
### Browsing files:
To browse directories, use the top left panel. To set a specific location, copy
paste a path into the path field at the top. You can set the default "Home" 
directory in the configuration dialog. Alternatively, you can use the auto
complete function of the path field to specify a path.

### Creating Bookmarks and Playlists
To create a bookmark, simply press the "star" button above the file browser panel.
This will create a bookmark for the directory you are currently in.

To create a playlist, press the "+" icon above the presets panel, and enter a name
for your playlist.

To add images to your playlist, select the images you wish to add, right click
and select the playlist you want to add them too from the "Add To Playlists" submenu.

### Thumbnail previews
To generate thumbnails of the images for the directory you are in, press the green
"refresh" icon on the thumbnail panels toolbar. This will generate new thumbnails
for all the valid images in your current directory.

If a thumbnail fails to load, you can select those items in the thumbnail viewer
and right click, then you can try a different method to generate using the options
in the "Generate Thumbnail" submenu.

To always have thumbnails generate, toggle on the last button (entitled "Always 
Generate Thumbnails") in the thumbnail viewer panel.

## Developer Notes

- Primary tested on Linux, additional testing on windows is in order
- This is a work in progress, but general framework is done.
- The thumbnail generation was built with intention of exandability, to add your
  own method, create a function object and add it to the dictionary in Lib.py. See example below.

    ```python
    def newMode(p, size):
        # do stuff
    convertModes["newMode"] = newMode
    ```


\* Only tested with Linux installs, Imagemagick should work with a windows install.