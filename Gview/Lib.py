#Gview LIB Module
#---------------------------------------------------
#Description: Image Thumbnail Browser for Mari
#Supported Versions: 2.6.x, 3.x
#Author: Ben Neall, Contact: bneall@gmail.com

from PySide import QtGui, QtCore
import os
import mari
import collections
import subprocess
import yaml

#MARI SETTINGS
MARI_FORMATS = mari.images.supportedReadFormats()
MARI_ICONS = mari.resources.path(mari.resources.ICONS)
MARI_USER = mari.resources.path(mari.resources.USER)

#GVIEW SETTINGS
VERSION = "0.9.3"
STYLE = "Tab"
HOME = os.path.expanduser("~")
ROOT = os.path.dirname(__file__)
ICONS = os.path.join(ROOT, "icons")
CACHE ='/var/tmp/thumbcache'
CONVERTMODE = "Qt"
BROWSERSTYLE = "List"
PRESETS = os.path.join(MARI_USER, "presets.yaml")
CONFIG = os.path.join(MARI_USER, "config.yaml")
THUMBSIZE = 200
THUMBPAD = 10
THUMBEXT = "png"
IMG_DEFAULT = os.path.join(ICONS, "placeholder.200x200.png")
IMG_CHECKER = os.path.join(ICONS, "graychecker.200x200.png")
PREFS = "GVIEW_PREFERENCES"
LISTSTYLE = "QListWidget, QListView, QTreeView { alternate-background-color: rgb(100, 100, 100); } "


#_____________________________________________
def loadSettings():

	if os.path.exists(CONFIG):
		with open(CONFIG, 'r') as yamlFile:
			config = yaml.load(yamlFile)

			global STYLE
			STYLE = config["STYLE"]

			global CACHE
			CACHE = config["CACHE"]

			global HOME
			HOME = config["HOME"]

			global BROWSERSTYLE
			BROWSERSTYLE = config["BROWSERSTYLE"]

	else:
		saveSettings()

#_____________________________________________
def saveSettings():
	gviewSettings = { "STYLE": STYLE, "CACHE": CACHE, "HOME": HOME, "BROWSERSTYLE": BROWSERSTYLE }

	#Save Settings
	with open(CONFIG, 'w') as yamlFile:
		yaml.safe_dump(gviewSettings, yamlFile, default_flow_style=False)


#_____________________________________________
def getFilePaths(path):
	''' Return list of files from given path'''
	normPath = os.path.normpath(path)
	fileList = []
	for filename in os.listdir(normPath):
		filepath = os.path.join(normPath, filename)
		if not filepath.startswith(".") and os.path.isfile(filepath):
			fileList.append(filepath)
	return fileList

#_____________________________________________
def pathProperties(path):
	'''Build list of valid image file property objects from path'''

	#Source Paths
	sourcePath = os.path.normpath(path)
	sourceDir = os.path.dirname(sourcePath)
	sourceFile = os.path.basename(sourcePath)
	sourceName, sourceExt = os.path.splitext(sourceFile)
	sourceExt = sourceExt.split(os.extsep)[-1]

	if sourceExt.lower() not in MARI_FORMATS:
		return

	#Thumbnail Paths
	if mari.app.version().isWindows():
		sourceDir = sourceDir.replace(':','')
	if mari.app.version().isLinux():
		sourceDir = sourceDir[1:]

	thumbnailDir = os.path.join(CACHE, sourceDir)
	thumbnailFile =  os.extsep.join([sourceName, THUMBEXT])
	thumbnailPath = os.path.join(thumbnailDir, thumbnailFile)

	PathProperties = collections.namedtuple('PathProperties', ['sourcePath', 'sourceDir', 'sourceFile', 'sourceName', 'sourceExt', 'thumbnailDir', 'thumbnailFile', 'thumbnailPath'])
	PathProperties = PathProperties(sourcePath, sourceDir, sourceFile, sourceName, sourceExt, thumbnailDir, thumbnailFile, thumbnailPath)
	return PathProperties

# _____________________________________________
def makeThumbnailDir(thumbnailPath):
	'''Makes directory for thumbnail if it doesnt already exist'''
	thumbnailDir = os.path.dirname(thumbnailPath)
	if os.path.exists(thumbnailDir):
		return True
	else:
		try:
			os.makedirs(thumbnailDir)
			return True
		except IOError:
			print "Could not create directory: ", thumbnailDir
			return False

#_____________________________________________
def makeThumbnail(convertFunc, p, force=False, size=0 ):
	'''Build thumbnails from given paths'''

	if not size:
		size = THUMBSIZE

	#Check if Source is newer then Thumbnail, return path
	if os.path.exists(p.thumbnailPath) and not force:
		inFileModified = os.path.getmtime(p.sourcePath)
		outFileModified = os.path.getmtime(p.thumbnailPath)
		if not inFileModified > outFileModified:
			return True
	else:
		if convertFunc(p, size):
			return True
		else:
			print "Unable to process image file: %s" % p.sourcePath


## CONVERT METHODS ##
#####################
convertModes = {}

## QT Method ##
#_____________________________________________
def convert_Qt(p, size):
	'''Converts thumbnails using Qt '''
	QtImgReader = QtGui.QImageReader()
	qt_formats = [str(f).lower() for f in QtGui.QImageReader.supportedImageFormats()]
	QtImgReader.setFileName(p.sourcePath)
	if QtImgReader.canRead() and p.sourceExt.lower() in qt_formats:
		sourceImage = QtGui.QImage(p.sourcePath)
		thumbImage = sourceImage.scaled(size, size, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)

		#Save out image file
		if makeThumbnailDir(p.thumbnailPath):
			if thumbImage.save(p.thumbnailPath, str(THUMBEXT), 75):
				return True

convertModes['Qt'] = convert_Qt

## Image Magick Method ##
#_____________________________________________
def convert_ImageMagick(p, size):
	'''Converts thumbnails using Image Magick '''
	if makeThumbnailDir(p.thumbnailPath):
		convert_cmd = 'convert "%s" -background transparent -layers merge -resize %sx%s -depth 8 -format %s "%s"' % (p.sourcePath, size, size, THUMBEXT, p.thumbnailPath)
		convert_proc = subprocess.Popen(convert_cmd, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = convert_proc.communicate()
		if not stderr:
			return True
		else:
			print stderr

convertModes['ImageMagick'] = convert_ImageMagick

## OIIO Method ##
#_____________________________________________
def convert_OIIO(p, size):
	'''Converts thumbnails using Image Magick '''
	if makeThumbnailDir(p.thumbnailPath):
		convert_cmd = 'oiiotool "%s" --resize %sx0 -o "%s"' % (p.sourcePath, size, p.thumbnailPath)
		convert_proc = subprocess.Popen(convert_cmd, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = convert_proc.communicate()
		if not stderr:
			print stdout
			return True
		else:
			print stderr

convertModes['OIIO'] = convert_OIIO