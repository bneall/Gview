#Gview GUI Module
#---------------------------------------------------
#Description: Image Thumbnail Browser for Mari
#Supported Versions: 2.6.x, 3.x
#Author: Ben Neall, Contact: bneall@gmail.com

from PySide import QtGui, QtCore
import mari
import os
import shutil
import yaml
import functools
import time

#GView Modules
import Lib as GV
reload(GV)

#Init Preferences
#----------------------------------------------
GV.loadSettings()
#----------------------------------------------


#------------------------------------------------#
# Popup Widget
#------------------------------------------------#
class GViewPopup(QtGui.QDialog):
    '''GView Custom Widget for Message Box style usage'''
    directoryChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(GViewPopup, self).__init__(parent)

        #Properties
        self.setWindowTitle('Gview')
        self.setMinimumWidth(250)
        self.setModal(True)

        #Layouts
        self.mainLayout = QtGui.QVBoxLayout()
        self.contentLayout = QtGui.QVBoxLayout()
        self.buttonLayout = QtGui.QHBoxLayout()
        self.setLayout(self.mainLayout)

        #Widgets
        self.rejectBtn = QtGui.QPushButton("Cancel")
        self.acceptBtn = QtGui.QPushButton("Ok")

        #Populate Layouts
        self.buttonLayout.addWidget(self.acceptBtn)
        self.buttonLayout.addWidget(self.rejectBtn)
        self.mainLayout.addLayout(self.contentLayout)
        self.mainLayout.addLayout(self.buttonLayout)

        #Connections
        self.acceptBtn.clicked.connect(self.accept)
        self.rejectBtn.clicked.connect(self.reject)

    def setMessage(self, text):
        label = QtGui.QLabel(text)
        self.contentLayout.insertWidget(0, label)

#------------------------------------------------#
# Gview Config Dialog
#------------------------------------------------#
class GviewPreferences(GViewPopup):
    '''GView Custom Widget for editing GView Preferences'''
    def __init__(self, parent=None):
        super(GviewPreferences, self).__init__(parent)

        self.setWindowTitle("Configure Gview")
        self.setMinimumWidth(400)

        #Cache Options
        cacheGroupLayout = QtGui.QVBoxLayout()
        cacheGroupBox = QtGui.QGroupBox("Cache")
        cacheGroupBox.setLayout(cacheGroupLayout)
        self.cacheEdit = QtGui.QLineEdit(GV.CACHE)
        self.cacheCopyOption = QtGui.QCheckBox("Copy Cache")
        self.cacheSizeLabel = QtGui.QLabel()
        cacheGroupLayout.addWidget(self.cacheEdit)
        cacheGroupLayout.addWidget(self.cacheCopyOption)
        cacheGroupLayout.addWidget(self.cacheSizeLabel)

        #Home Directory
        homeGroupLayout = QtGui.QHBoxLayout()
        homeGroupBox = QtGui.QGroupBox("Home")
        homeGroupBox.setLayout(homeGroupLayout)
        self.homeEdit = QtGui.QLineEdit(GV.HOME)
        homeGroupLayout.addWidget(self.homeEdit)

        #Style Widget
        styleGroupLayout = QtGui.QHBoxLayout()
        styleGroupBox = QtGui.QGroupBox("Widget Style")
        styleGroupBox.setLayout(styleGroupLayout)
        self.styleCombo = QtGui.QComboBox()
        self.styleCombo.addItems(["Palette","Tab"])
        self.styleCombo.setCurrentIndex(self.styleCombo.findText(GV.STYLE))
        self.styleCombo.setToolTip("Set the widget style of Gview")
        styleGroupLayout.addWidget(self.styleCombo)

        #Conversion Option
        conversionGroupLayout = QtGui.QHBoxLayout()
        conversionGroupBox = QtGui.QGroupBox("Conversion Default")
        conversionGroupBox.setLayout(conversionGroupLayout)
        self.convertCombo = QtGui.QComboBox()
        self.convertCombo.addItems(GV.convertModes.keys())
        self.convertCombo.setCurrentIndex(self.convertCombo.findText(GV.CONVERTMODE))
        self.convertCombo.setToolTip("Set the default conversion method")
        conversionGroupLayout.addWidget(self.convertCombo)

        #Browser Style Option
        browserStyleLayout = QtGui.QHBoxLayout()
        browserStyleGroupBox = QtGui.QGroupBox("Browser Style")
        browserStyleGroupBox.setLayout(browserStyleLayout)
        self.browserStyleCombo = QtGui.QComboBox()
        self.browserStyleCombo.addItems(["Tree", "List"])
        self.browserStyleCombo.setCurrentIndex(self.browserStyleCombo.findText(GV.BROWSERSTYLE))
        self.browserStyleCombo.setToolTip("Set the file browser widget style")
        browserStyleLayout.addWidget(self.browserStyleCombo)

        self.mainLayout.insertWidget(0, browserStyleGroupBox)
        self.mainLayout.insertWidget(0, conversionGroupBox)
        self.mainLayout.insertWidget(0, styleGroupBox)
        self.mainLayout.insertWidget(0, cacheGroupBox)
        self.mainLayout.insertWidget(0, homeGroupBox)

        self.accepted.connect(self.setPreferences)

        #init
        self.calculateCacheSize()

    #_____________________________________________
    def calculateCacheSize(self):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(GV.CACHE):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        gb = float(total_size) / 1024 / 1024 / 1024
        self.cacheSizeLabel.setText("Cache Size: %s GB" % format(gb, '.2f'))

    #_____________________________________________
    def setPreferences(self):
        reset = False

        #Update Home Directory
        inputHomeDir = self.homeEdit.text()
        if inputHomeDir != GV.HOME:
            if os.path.exists(inputHomeDir):
                GV.HOME = inputHomeDir
            else:
                #LOG ERROR
                pass

        #Update Cache Directory
        inputCacheDir = self.cacheEdit.text()
        if inputCacheDir != GV.CACHE:
            if os.access(os.path.dirname(inputCacheDir), os.W_OK):
                if self.cacheCopyOption.isChecked():
                    self.copyCache(GV.CACHE, inputCacheDir)
                GV.CACHE = inputCacheDir
            else:
                #LOG ERROR
                pass

        #Update Widget Style
        inputStyle = self.styleCombo.currentText()
        if inputStyle != GV.STYLE:
            GV.STYLE = inputStyle
            reset = True

        #Update Conversion Default
        convertMode = self.convertCombo.currentText()
        if convertMode != GV.CONVERTMODE:
            GV.CONVERTMODE = convertMode

        #Update Browser Style Default
        browserStyle = self.browserStyleCombo.currentText()
        if browserStyle != GV.BROWSERSTYLE:
            GV.BROWSERSTYLE = browserStyle
            reset = True

        #Write changes
        GV.saveSettings()

        if reset:
            loadUI()

    #_____________________________________________
    def copyCache(self, source, target):
        '''Utility for copying cache contents from one location to another'''
        for root, dirs, files in os.walk(source, topdown=True):
            for filename in files:
                sourcePath = os.path.join(root, filename)
                targetPath = sourcePath.replace(source, target)
                targetDir = os.path.dirname(targetPath)

                try:
                    if not os.path.exists(targetDir):
                        os.makedirs(targetDir)
                except OSError, e:
                    #LOG ERROR
                    pass

                try:
                    #-- Copy
                    shutil.copyfile(sourcePath, targetPath)
                except  IOError, e:
                    #LOG ERROR
                    pass


#------------------------------------------------#
# File Browser Widget
#------------------------------------------------#
class GViewFile(QtGui.QWidget):
    '''GView Custom File Browser Widget'''
    directoryChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(GViewFile, self).__init__(parent)

        #Layouts
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0,0,0,0)
        self.setLayout(mainLayout)

        #Widget -- FileSystem View/Model
        self.fileModel = QtGui.QFileSystemModel()
        self.fileModel.setRootPath(QtCore.QDir.rootPath())
        self.fileModel.setFilter( QtCore.QDir.Dirs | QtCore.QDir.NoDot )

        #Browser Viewer Style
        if GV.BROWSERSTYLE == "Tree":
            self.fileViewer = QtGui.QTreeView()
            self.fileViewer.setExpandsOnDoubleClick(False)

        if GV.BROWSERSTYLE == "List":
            self.fileViewer = QtGui.QListView()

        self.fileViewer.setModel(self.fileModel)
        self.fileViewer.setAlternatingRowColors(True)

        if GV.BROWSERSTYLE == "Tree":
            self.fileViewer.hideColumn(1)
            self.fileViewer.hideColumn(2)
            self.fileViewer.hideColumn(3)

        #Widget -- Completer
        completer = QtGui.QCompleter()
        completer.setModel(self.fileModel)

        #Widget -- Path Edit
        self.pathEdit = QtGui.QLineEdit()
        self.pathEdit.setCompleter(completer)

        #ToolBar
        self.navToolBar = QtGui.QToolBar()
        #Navigate Home
        navHomeIcon = QtGui.QIcon(os.path.join(GV.MARI_ICONS, "Home.png"))
        navUpAction = self.navToolBar.addAction(navHomeIcon, "Home Directory", self.setDirHome)

        #Populate Layouts
        mainLayout.addWidget(self.navToolBar)
        mainLayout.addWidget(self.fileViewer)

        #Stylesheet
        self.setStyleSheet(GV.LISTSTYLE)

        #Connections
        self.fileViewer.doubleClicked.connect(self.setDirFromIndex)
        self.pathEdit.returnPressed.connect(self.setDirFromEdit)

        #Init
        self.setDirHome()

    #_____________________________________________
    def setDirHome(self):
        self.setDirFromPath(GV.HOME)

    #_____________________________________________
    def setDirFromPath(self, path):
        path = os.path.normpath(path)
        index = self.fileModel.index(path)
        self.fileViewer.setRootIndex(index)
        self.directoryChanged.emit(path)
        self.pathEdit.setText(path)
        self.currentPath = path

    #_____________________________________________
    def setDirFromEdit(self):
        editPath = self.pathEdit.text()
        if os.path.exists(editPath):
            self.setDirFromPath(editPath)

    #_____________________________________________
    def setDirFromIndex(self, index):
        #Note: Redundant, but only way it would work
        path = os.path.normpath( self.fileModel.filePath(index) )
        index = self.fileModel.index(path)
        self.fileViewer.setRootIndex(index)
        self.directoryChanged.emit(path)
        self.pathEdit.setText(path)
        self.currentPath = path


#------------------------------------------------#
# Image List Widget
#------------------------------------------------#
class ImageList(QtGui.QListWidget):
    '''GView Custom QListWidget for thumbnail layout'''
    def __init__(self):
        super(ImageList, self).__init__()

        #Properties
        self.setViewMode(self.IconMode)
        self.setLayoutMode(self.Batched)
        self.setFlow(self.LeftToRight)
        self.setSortingEnabled(True)
        self.setWrapping(True)
        self.setResizeMode(self.Adjust)
        self.setDragEnabled(False)
        self.setUniformItemSizes(True)
        self.setSelectionMode(self.ExtendedSelection)

        #Widgets
        self.zoomInLabel = QtGui.QLabel()
        self.zoomInLabel.setPixmap(QtGui.QPixmap(os.path.join(GV.ICONS, "zoomin.16x16.png")))
        self.zoomOutLabel = QtGui.QLabel()
        self.zoomOutLabel.setPixmap(QtGui.QPixmap(os.path.join(GV.ICONS, "zoomout.16x16.png")))
        self.iconSizeSlider = QtGui.QSlider()
        self.iconSizeSlider.setRange(50,  GV.THUMBSIZE)
        self.iconSizeSlider.setValue(GV.THUMBSIZE)
        self.iconSizeSlider.setOrientation(QtCore.Qt.Horizontal)
        self.iconSizeSlider.setFixedWidth(150)

        #Context Menu
        self.contextMenu = QtGui.QMenu()
        sendToMariAction = self.contextMenu.addAction("Import")
        sendToMariAction.triggered.connect(self.importSelected)
        self.contextMenu.addSeparator()
        self.convertWithMenu = self.contextMenu.addMenu("Generate Thumbnail")
        self.addToPlaylistMenu = self.contextMenu.addMenu("Add to Playist")

        #Stylesheet
        self.setStyleSheet("QListWidget::item:selected {background-color: rgba(255,133,29,50); border: 1px solid rgba(255,133,29,200);} ")

        #Connections
        self.iconSizeSlider.valueChanged.connect(self.setItemSize)

        #Init
        self.gridSize = QtCore.QSize()
        self.iconSize = QtCore.QSize()
        self.setItemSize()

    #_____________________________________________
    def contextMenuEvent(self, event):
        self.contextMenu.exec_(event.globalPos())

    #_____________________________________________
    def getItems(self, selection=False):
        '''Return list of items from a listwidget'''
        items = []
        if not selection:
            for i in xrange(self.count()):
                item = self.item(i)
                items.append(item)
        else:
            for item in self.selectedItems():
                items.append(item)
        return items

    #_____________________________________________
    def setItemSize(self, size=0, pad=0):
        '''Set Thumbnail and Grid size'''

        #Defaults
        if not pad:
            pad = GV.THUMBPAD
        if not size:
            size = GV.THUMBSIZE

        #Icon
        self.iconSize.setWidth(size)
        self.iconSize.setHeight(size)
        self.setIconSize(self.iconSize)

        #Grid
        self.gridSize.setWidth(size + pad + 2)
        self.gridSize.setHeight(size + pad + 20)
        self.setGridSize(self.gridSize)

        allCurrentItems = self.getItems()
        if allCurrentItems:
            for item in allCurrentItems:
                item.setSizeHint(QtCore.QSize(size + 2, size + 20))

    #_____________________________________________
    def createItem(self, sourcePath, sourceName):

        #New thumbnail item
        newItem = QtGui.QListWidgetItem(sourceName)
        newItem.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignBottom)
        newItem.setSizeHint(QtCore.QSize(self.iconSize.width() + 2, self.iconSize.height() + 20))
        newItem.setBackground(QtGui.QBrush(QtGui.QColor(100,100,100)))
        newItem.setIcon(QtGui.QIcon(QtGui.QPixmap(GV.IMG_DEFAULT)))
        newItem.setToolTip(sourcePath)
        self.addItem(newItem)

        #MARI Process Events
        mari.app.processEvents()

    #_____________________________________________
    def setItemThumbnail(self, sourcePath, thumbnailPath):
        for item in self.getItems():
            if item.toolTip() == sourcePath:

                #Pixmap using Pixmap Cache
                thumbnailPixmap = QtGui.QPixmap()
                if not QtGui.QPixmapCache.find(thumbnailPath, thumbnailPixmap):
                    thumbnailPixmap.load(thumbnailPath)
                    QtGui.QPixmapCache.insert(thumbnailPath, thumbnailPixmap)

                #Add background for thumbnails with transparency
                if thumbnailPixmap.hasAlpha():
                    checker_pixmap = QtGui.QPixmap(GV.IMG_CHECKER)
                    item.setBackground(QtGui.QBrush(checker_pixmap))

                #Set Icon
                item.setIcon(QtGui.QIcon(thumbnailPixmap))

                #MARI Process Events
                mari.app.processEvents()

    #_____________________________________________
    def importSelected(self):
        selectedItems = self.getItems(selection=True)
        if selectedItems:
            imagePaths = [ i.toolTip() for i in selectedItems ]
            for path in imagePaths:
                mari.images.load(path)


#------------------------------------------------#
# Preset List Widget
#------------------------------------------------#
class GviewPresetList(QtGui.QListWidget):
    presetsModified = QtCore.Signal()
    def __init__(self, parent=None):
        super(GviewPresetList, self).__init__(parent)

        #Properties
        self.setSelectionMode(self.ExtendedSelection)
        self.setAlternatingRowColors(True)

        #ToolBar
        self.optionToolBar = QtGui.QToolBar()
        addPlaylistIcon = QtGui.QIcon(os.path.join(GV.MARI_ICONS, "SnapshotAll.16x16.png"))
        addPlaylistAction = self.optionToolBar.addAction(addPlaylistIcon, "New Playlist", self.addPlaylist)

        #Stylesheet
        self.setStyleSheet(GV.LISTSTYLE)

    #_____________________________________________
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self.removeSelectedItems()

    #_____________________________________________
    def allItems(self):
        return [ self.item(row) for row in range(self.count()) ]

    #_____________________________________________
    def itemNameExists(self, name):
        for item in self.allItems():
            if item.text() == name:
                return True

    #_____________________________________________
    def openPresets(self):
        if os.path.exists(GV.PRESETS):
            with open(GV.PRESETS, 'r') as yamlFile:
                presets = yaml.load(yamlFile)
                for preset in presets:
                    self.addNewPresetItem(preset["name"], preset["type"], preset["value"])

    #_____________________________________________
    def savePresets(self):
        presets = []
        for item in self.allItems():
            data = item.data(32)
            preset = data.copy()
            preset["name"]=item.text()
            presets.append(preset)

        #Save Settings
        with open(GV.PRESETS, 'w') as yamlFile:
            yaml.safe_dump(presets, yamlFile, default_flow_style=False)

    #_____________________________________________
    def addPlaylist(self):

        nameEdit = QtGui.QLineEdit()
        popupDialog = GViewPopup(self)
        popupDialog.contentLayout.addWidget(nameEdit)
        popupDialog.setMessage("Playlist Name:")

        def setName():
            if not nameEdit.text():
                return
            else:
                self.addNewPresetItem( name=nameEdit.text(), presetType="playlist", value=set() )

        popupDialog.accepted.connect(setName)
        popupDialog.show()

    #_____________________________________________
    def addNewPresetItem(self, name, presetType, value):

        if self.itemNameExists(name):
            popupDialog = GViewPopup(self)
            popupDialog.setMessage("Preset with that name already exists!")
            popupDialog.show()
            return

        #New Item
        itemDict = {"name": name, "type": presetType, "value": value}
        newItem = QtGui.QListWidgetItem(name)
        newItem.setData(32, itemDict)

        #playlist tint
        if presetType == "playlist":
            newItem.setBackground(QtGui.QBrush(QtGui.QColor(100,200,0,50)))

        preset_pixmap = QtGui.QPixmap(os.path.join(GV.MARI_ICONS, "FolderClosed.16x16.png"))
        newItem.setIcon(QtGui.QIcon(preset_pixmap))

        #Add New Item
        self.addItem(newItem)
        self.presetsModified.emit()
        self.savePresets()

    #_____________________________________________
    def getPlaylists(self):
        playlists = []
        for item in self.allItems():
            itemdata = item.data(32)
            if itemdata["type"] == "playlist":
                name = item.text()
                playlists.append(name)
        return playlists

    #_____________________________________________
    def updatePlaylistItem(self, name,  value):
        #Find By Name
        for item in self.allItems():
            itemdata = item.data(32)
            if itemdata["type"] == "playlist" and item.text() == name:
                    newData = itemdata.copy()
                    newData["value"] = newData["value"].union(value)
                    item.setData(32, newData)
        self.presetsModified.emit()
        self.savePresets()

    #_____________________________________________
    def removeSelectedItems(self):

        selectedItems = self.selectedItems()
        if not selectedItems:
            return

        popupDialog = GViewPopup(self)
        popupDialog.setMessage("Remove Selected Items?")

        def removeItems():
            for item in selectedItems:
                self.takeItem(self.row(item))
                del item
            self.presetsModified.emit()
            self.savePresets()

        popupDialog.accepted.connect(removeItems)
        popupDialog.show()



#------------------------------------------------#
# Viewer Widget
#------------------------------------------------#
class GView(QtGui.QWidget):
    '''Main Custom Widget for GView '''
    def __init__(self, parent=None):
        super(GView, self).__init__(parent)

        #Layouts
        mainLayout = QtGui.QVBoxLayout()
        viewOptionLayout = QtGui.QHBoxLayout()
        presetOptionLayout = QtGui.QHBoxLayout()
        statusBarLayout = QtGui.QHBoxLayout()
        self.setLayout(mainLayout)

        #Widgets
        self.filebrowser = GViewFile()
        self.presets = GviewPresetList()
        self.imageList = ImageList()

        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setHidden(True)

        #Bookmark
        bookmarkIcon = QtGui.QIcon(os.path.join(GV.ICONS, "star.16x16.png"))
        bookmarkAction = self.filebrowser.navToolBar.addAction(bookmarkIcon, "Bookmark current directory", self.add_bookmark)
        #ToolBar
        viewerToolBar = QtGui.QToolBar()
        #Load Thumbnails
        loadThumbnailIcon = QtGui.QIcon(os.path.join(GV.ICONS, "refresh.32x32.png"))
        loadThumbnailAction = viewerToolBar.addAction(loadThumbnailIcon, "Reload Thumbnails", self.load_fromView)
        #Config
        configIcon = QtGui.QIcon(os.path.join(GV.MARI_ICONS, "Preference.png"))
        configAction = viewerToolBar.addAction(configIcon, "Configure Gview", self.setConfig)
        #Auto Load
        autoLoadIcon = QtGui.QIcon(os.path.join(GV.MARI_ICONS, "ViewImage.png"))
        self.autoLoadAction = viewerToolBar.addAction(autoLoadIcon, "Always Generate Thumbnails")
        self.autoLoadAction.setCheckable(True)

        #Sidebar Container
        sideBarContainer = QtGui.QWidget()
        sideBarContainer.setMinimumWidth(220)
        sideBarLayout = QtGui.QVBoxLayout()
        sideBarLayout.setContentsMargins(0,0,0,0)
        sideBarContainer.setLayout(sideBarLayout)
        sideBarLayout.addWidget(self.filebrowser)
        sideBarLayout.addLayout(presetOptionLayout)
        sideBarLayout.addWidget(self.presets)

        #View Container
        viewContainer = QtGui.QWidget()
        viewLayout = QtGui.QVBoxLayout()
        viewLayout.setContentsMargins(0,0,0,0)
        viewContainer.setLayout(viewLayout)
        viewLayout.addLayout(viewOptionLayout)
        viewLayout.addWidget(self.imageList)

        #Viewer Splitter
        viewSplitter = QtGui.QSplitter()
        viewSplitter.addWidget(sideBarContainer)
        viewSplitter.addWidget(viewContainer)
        viewSplitter.setSizes([220, 800])
        viewSplitter.setHandleWidth(6)

        #Assign Layouts
        viewOptionLayout.addWidget(viewerToolBar)
        viewOptionLayout.addStretch()
        viewOptionLayout.addWidget(self.imageList.zoomOutLabel)
        viewOptionLayout.addWidget(self.imageList.iconSizeSlider)
        viewOptionLayout.addWidget(self.imageList.zoomInLabel)
        presetOptionLayout.addWidget(self.presets.optionToolBar)
        statusBarLayout.addWidget(self.progressBar)
        mainLayout.addWidget(self.filebrowser.pathEdit)
        mainLayout.addWidget(viewSplitter)
        mainLayout.addLayout(statusBarLayout)

        #Connections
        self.presets.itemDoubleClicked.connect(self.load_preset)
        self.presets.presetsModified.connect(self.load_playListMenus)
        self.filebrowser.directoryChanged.connect(self.load_fromPath)

        #Init
        self.presets.openPresets()
        self.load_convertMenus()
        self.load_fromPath()

    #_____________________________________________
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.exitLoad()

    #_____________________________________________
    def setConfig(self):
        configPopup = GviewPreferences(self)
        configPopup.show()

    #_____________________________________________
    def exitLoad(self):
        '''Stop thumbnail generation'''
        self.exit = True
        self.progressBar.setHidden(True)

    #_____________________________________________
    def add_toPlaylist(self, name):
        paths = []
        for item in self.imageList.selectedItems():
            path = item.toolTip()
            paths.append(path)
        self.presets.updatePlaylistItem(name, paths)

    #_____________________________________________
    def add_bookmark(self):
        dirName = os.path.basename(self.filebrowser.currentPath)
        self.presets.addNewPresetItem( name=dirName, presetType="bookmark", value=self.filebrowser.currentPath)

    #_____________________________________________
    def progress_update(self, step):
        self.progressBar.setValue(self.progressBar.value() + step)

    #_____________________________________________
    def progress_reset(self, maxValue):
        self.progressBar.reset()
        self.progressBar.setMaximum(maxValue-1)

    #_____________________________________________
    def load_convertMenus(self):
        for convertMode in GV.convertModes:
            action = self.imageList.convertWithMenu.addAction(convertMode)
            action_cmd = functools.partial(self.load_fromView, convertMode)
            action.triggered.connect(action_cmd)

    #_____________________________________________
    def load_playListMenus(self):
        playlists = self.presets.getPlaylists()
        if not playlists:
            self.imageList.addToPlaylistMenu.setDisabled(True)
            return
        else:
            self.imageList.addToPlaylistMenu.setDisabled(False)
            self.imageList.addToPlaylistMenu.clear()
            for playlist in playlists:
                action = self.imageList.addToPlaylistMenu.addAction(playlist)
                action_pixmap = QtGui.QPixmap(os.path.join(GV.MARI_ICONS, "FolderClosed.16x16.png"))
                action.setIcon(QtGui.QIcon(action_pixmap))
                action_cmd = functools.partial(self.add_toPlaylist, playlist)
                action.triggered.connect(action_cmd)

    #_____________________________________________
    def load_thumbnail(self, p, mode, force=False):
        ''' p as pathproperty object, if exists flag, will grab existing thumbnails and not generate new ones'''

        if not self.exit:
            #Force regenerate
            if force:
                if GV.makeThumbnail(GV.convertModes[mode], p, force):
                    self.imageList.setItemThumbnail(p.sourcePath, p.thumbnailPath)
                    self.progress_update(1)
            #Use existing
            if os.path.exists(p.thumbnailPath):
                self.imageList.setItemThumbnail(p.sourcePath, p.thumbnailPath)
                self.progress_update(1)
            #Generate if autoload is checked
            else:
                if self.autoLoadAction.isChecked():
                    if GV.makeThumbnail(GV.convertModes[mode], p, force):
                        self.imageList.setItemThumbnail(p.sourcePath, p.thumbnailPath)
                        self.progress_update(1)

    # _____________________________________________
    def load_preset(self, item):
        '''Loads bookmark path from bookmark item'''
        itemdata = item.data(32)
        if itemdata["type"] == "bookmark":
            bookmarkPath = itemdata["value"]
            self.filebrowser.setDirFromPath(bookmarkPath)
        if itemdata["type"] == "playlist":
            playlistPaths = itemdata["value"]
            self.load_fromPath(pathList=playlistPaths)

    #_____________________________________________
    def load_fromPath(self, path=None, pathList=None):
        '''Loads new items and from path or list of paths given'''
        self.exit = False

        ##List of file properties
        if not path:
            path = self.filebrowser.pathEdit.text()

        if not pathList:
            pathList = GV.getFilePaths(path)

        pathPropertiesList = [ GV.pathProperties(f) for f in pathList if GV.pathProperties(f) ]

        ##Clear image list
        self.imageList.clear()

        ##Reset Progress bar
        self.progress_reset(len(pathPropertiesList))

        ##Build Items first
        for p in pathPropertiesList:
            self.imageList.createItem(p.sourcePath, p.sourceName)

        ##load_fromPath thumbnails next
        self.progressBar.setHidden(False)
        for p in pathPropertiesList:
            self.load_thumbnail(p, GV.CONVERTMODE)
        self.progressBar.setHidden(True)

    #_____________________________________________
    def load_fromView(self, mode=None):
        self.exit = False

        if not mode:
            mode = GV.CONVERTMODE

        items = self.imageList.getItems(selection=True)
        if not items:
            items = self.imageList.getItems()

        ##Reset Progress bar
        self.progress_reset(len(items))
        self.progressBar.setHidden(False)

        ##Iterate over all listwidget items
        for item in items:
            filepath = item.toolTip()
            p = GV.pathProperties(filepath)
            self.load_thumbnail(p, mode, force=True)
        self.progressBar.setHidden(True)


##LOADER
##_______________________________________________________________________
def loadUI():

    #Remove Widget
    try:
        mari.app.removeTab("Gview")
    except Exception, e:
        print e
    try:
        mari.palettes.remove("Gview")
    except Exception, e:
        print e

    #Load Widget
    gviewer = GView()
    if GV.STYLE == "Tab":
        mari.app.addTab("Gview", gviewer)
    elif GV.STYLE == "Palette":
        mari.palettes.create("Gview", gviewer)